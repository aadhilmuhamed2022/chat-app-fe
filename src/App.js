import React, { useState, useEffect } from 'react';
import './App.css';
import { io } from 'socket.io-client';



function App() {
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const jioned = useState(false);
  const [name, setName] = useState('');
  const [typing, setTyping] = useState('')


  const socket = io("http://localhost:4500");
  useEffect(() => {
    // Receive 'message' event from the server
    socket.on('message', (message) => {
      console.log("emit from server",message);
      setMessages((prevMessages) => [...prevMessages,message]);
    });
  }, []);

  const handleSend = () => {
    // Emit 'createMessage' event to the server
    socket.emit('message', { message:message });
    setMessage('');
  };
  console.log(message);
  console.log("messages",messages);


  const join = () =>{
    socket.emit('join', { name:name.value }, () => {
      jioned.value = true;
    });
  }


  let timeout;
  const emittyping = () => {
    socket.emit('typing',{isTyping:true});
    timeout = setTimeout(() =>{
      socket.emit('typing',{isTyping:false});
  
    },1500);
    



  }



  
  return (
    <div className='container'>

      {/* <div if="!joined" >setName
      
      
      </div> */}
     <div className='message-container'>
       
     
     
          {messages.map((msg, index) => (
           
            <p key={index}>{msg?.message }</p>
          ))}
         </div>
      <div className='input-container'>
        <div className='input-container input'>
        <input
          placeholder='send message'
          value={message}
          onChange={(e) => setMessage(e.target.value)}
        />
        
      
        <button onClick={handleSend}>Send</button>
        </div>
      </div>
     
    </div>
  );
}

export default App;
